package cn.caizhenhang.crawler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import java.time.LocalDate;

@Component
public class CrawlerTask {

    Logger logger = LoggerFactory.getLogger(CrawlerTask.class);

    @Autowired
    App2 app;

    //半个小时执行一遍
    //markmarkmarkmark 正式
//    @Scheduled(cron = "0 0/30 * * * ?")//这里不是30分一次，是1:30这样一次
//    @Scheduled(cron = "0 0 */4 * * *")//每三个小时做一次

//    markmarkmarkmark客户端一次性测试用
//    @Scheduled(cron = "0 0 12 * * ?")

//    markmarkmarkmark服务器一运行启动一次，跑完了才会启动第二次
    @Scheduled(fixedDelay = 7200000)

//    markmarkmarkmark服务器一运行启动一次仅此一次  用于测试
//    @Scheduled(fixedDelay = 1000000000)
    public void start() {
        logger.info("执行定时任务>>>");
        System.out.println("执行定时任务>>>");
        //todo 读取所有userlist的账号密码，进行爬取。
        try {
            String userListStr = CommonUtil.fileToString(CommonUtil.kUserListFilePath);
            JSONArray ja = new JSONArray(userListStr);
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            String startTime = dtf.format(now);
            for (int i = 0; i < ja.length(); i++) {
                JSONObject jo = ja.getJSONObject(i);
                String username = jo.getString("username");
                String passwd = jo.getString("passwd");
                System.out.println("start chrome!!!!!!!!!11");
                app.start(username, passwd, startTime);
            }

        } catch (JSONException e) {
            logger.warn("crawler task execution failure, {}", e.getMessage());
        }

        logger.info("结束定时任务>>>");
        System.out.println("结束定时任务>>>");


    }
}
