package cn.caizhenhang.crawler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ServletComponentScan
public class CrawlerApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(CrawlerApplication.class, args);

//		context.getBean(CrawlerTask.class).start();//markmarkmarkmarkmark 删除

	}

}

