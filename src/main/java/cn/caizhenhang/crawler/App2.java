package cn.caizhenhang.crawler;

import net.sourceforge.tess4j.TesseractException;
import okhttp3.*;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.json.JSONArray;
import org.json.JSONException;
import org.openqa.selenium.*;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpLogging;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
    import java.util.ArrayList;
    import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class App2 {
    //    private static String userName = "123";
//    private static String userPasswd = "456s";
//    private static String userName = "ZHAORUNKANG";
//    private static String userPasswd = "YX13702588082";
    private static final String loginUrl = "http://app.singlewindow.cn/cas/login?_local_login_flag=1&service=http://app.singlewindow.cn/cas/jump.jsp%3FtoUrl%3DaHR0cDovL2FwcC5zaW5nbGV3aW5kb3cuY24vY2FzL29hdXRoMi4wL2F1dGhvcml6ZT9jbGllbnRfaWQ9MTM2NyZyZXNwb25zZV90eXBlPWNvZGUmcmVkaXJlY3RfdXJpPWh0dHAlM0ElMkYlMkZ3d3cuc2luZ2xld2luZG93LmdkLmNuJTJGcG9ydGFsJTJGc3dPYXV0aExvZ2luLmFjdGlvbiUzRnNlcnZpY2VVcmwlM0Q=&localServerUrl=http://www.singlewindow.gd.cn&localDeliverParaUrl=/portal/getSWParams.action&isLocalLogin=1&localLoginLinkName=5pys5Zyw55m75b2V&localLoginUrl=L3BvcnRhbC9zd09hdXRoTG9naW4uYWN0aW9u&colorA1=FFFFFF&colorA2=60,60,61, 0.5&localRegistryUrl=aHR0cDovL3N3Y2FzLnNpbmdsZXdpbmRvdy5nZC5jbi9nZHN3L2V0cHNSZWdpc3Rlci9ldHBzUmVnaXN0ZXJOb0NhcmRQYWdlLmxlYWQ=";
    private static final String kHost = "http://www.singlewindow.gd.cn";
    private static final String HTTPS_HOST = "https://www.singlewindow.gd.cn";
    private static final String secondPageTitle = "个人主页";
    private static final String thirdPageTitle = "中国国际贸易单一窗";

    private static final long kWaitTimeShort = 40;
    private static final long kWaitTimeLong = 20;//todo markmark 这里设置成20
    private static final long kWaitTimeImplicit = 5;
    private static final int kLoginTryTimes = 10;

    private static final long kTestWaitTime = 2000;//todo markmark 这里设置成2000，停留页面的时间，测试用

    private static final boolean DEBUG = true;

    @Autowired
    private OkHttpClient okHttpClient;

    private WebDriver driver = null;
    String cookieToSet = "";
    private String mStartTime;

    Logger logger = LoggerFactory.getLogger(App2.class);

    public void start(String userName, String userPasswd, String startTime) {
        mStartTime = startTime;
//        setSystemConfig();

//        ChromeOptions options = new ChromeOptions();
//        options.setBinary("C:\\develop\\tools\\chromedriver\\chromedriver.exe");

//        System.setProperty("webdriver.chrome.driver", "C:\\develop\\tools\\chromedriver\\chromedriver.exe");//chromedriver服务地址
        System.setProperty("webdriver.chrome.driver", "C:\\CAI\\chromedriver.exe");//chromedriver服务地址
        driver = new ChromeDriver();
        driver.get(loginUrl);//打开指定的网站

        try {
            if(DEBUG == true){
                Thread.sleep(2000);
            }else{
                Thread.sleep(5000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.get(loginUrl);//打开指定的网站


        Set<Cookie> cookieSet = driver.manage().getCookies();
        cookieToSet = "";
        for (Cookie c : cookieSet) {
            cookieToSet = cookieToSet + c.getName() + "=" + c.getValue() + ";";
        }

        System.out.println("------------- login cookies ===============");
        System.out.println(cookieSet);
        driver.manage().timeouts().implicitlyWait(kWaitTimeImplicit, TimeUnit.SECONDS);


        String captcha = null;
        try {
            WebElement image = driver.findElement(By.id("vcode"));
            String vcodeSrc = image.getAttribute("src");
            BufferedImage saveImage = null;
            boolean isLoginSuccess = false;
            for (int j = 0; j < kLoginTryTimes; j++) {
                WebElement accountTag = new WebDriverWait(driver, kWaitTimeLong).until(new ExpectedCondition<WebElement>() {
                    @NullableDecl
                    @Override
                    public WebElement apply(@NullableDecl WebDriver webDriver) {
                        return driver.findElement(By.id("swy"));
                    }
                });

                WebElement username = driver.findElement(By.id("swy"));
                username.clear();
                username.sendKeys(userName);
                WebElement password = driver.findElement(By.id("swm2"));
                password.clear();
                password.sendKeys(userPasswd);
                ///markmarkmarkmark
//                private void downloadFile(String url, String filePath, String cookieToSet) throws JSONException{

                URL imageUrl = new URL(vcodeSrc + "?r=" + Math.random());

                System.out.println("url = " + imageUrl.toString());
                URLConnection connection = imageUrl.openConnection();
                connection.setRequestProperty("Cookie", cookieToSet);
                saveImage = ImageIO.read(connection.getInputStream());


                /**
                 * 这是自己带着cookies请求回来的验证码图像。
                 */
                File outputFile = new File("downloadedimage.png");
                ImageIO.write(saveImage, "png", outputFile);


                /**
                 * 这是对验证码图像进行处理
                 */
                ImageUtil.binaryzation(saveImage);
                ImageIO.write(saveImage, "png", new File("image.png"));
                captcha = OCR.getTesseract().doOCR(saveImage).replaceAll(" ", "").trim();

                System.out.println("captcha = " + captcha);
                System.out.println(driver.manage().getCookies());

                WebElement captchaEdit = driver.findElement(By.id("verifyCode"));
                captchaEdit.clear();
                captchaEdit.sendKeys(captcha);

                // 左击实现（和元素的click类似）
                Actions action = new Actions(driver);
                WebElement test1item = driver.findElement(By.id("sub3"));
                action.click(test1item).perform();

                if (driver.getTitle().equals(secondPageTitle)) {
                    System.out.println("登陆成功！！！！！！！！！");
                    isLoginSuccess = true;
                    break;
                }
            }

            if (isLoginSuccess) {

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                List<WebElement> eList = driver.findElements(By.id("AppPersonalID"));

                JavascriptExecutor jsx = (JavascriptExecutor)driver;
                jsx.executeScript("arguments[0].scrollIntoView()", eList.get(0));
                /**
                 * 切换框架iframe，因为不切换findelement是找不到那个框架的元素的。
                 */

                driver.switchTo().frame(1).switchTo().frame(0);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

 

                /**
                 * 把鼠标hover在“货物申报”按钮上面，因为selenium只能找到眼真正看到的按钮，这样后面findelemnt才找到
                 */
                List<WebElement> navEleList = driver.findElement(By.id("ywsb")).findElement(By.className("asul"))
                        .findElements(By.className("yx"));

                for (WebElement e : navEleList) {
                    if (e.getText().equals("货物申报")) {
                        Actions act = new Actions(driver);
                        act.moveToElement(e);
                        act.build().perform();
                        System.out.println("货物申报 移动了！！！！！！");

                        break;
                    }
                }



                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                /**
                 * 点击“货物申报”进去查询单子界面。
                 */
//                List<WebElement> seleList = driver.findElement(By.id("ywsb")).findElement(By.cssSelector(".aslia.yw6"))
//                        .findElements(By.className("nrmc"));
                List<WebElement> seleList = driver.findElement(By.id("ywsb")).findElement(By.cssSelector(".aslia.yw6"))
                        .findElements(By.tagName("a"));
                for (WebElement e : seleList) {
                    if (e.getText().equals("货物申报")) {
                        e.click();
                        break;
                    }
                }


                try {
                    if(DEBUG == true){
                        Thread.sleep(5000);
                    }else{
                        Thread.sleep(15000);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                boolean isTargetWindowOpened = false;

                /**
                 * nav-label 点击“数据查询/统计”
                 */
                /**
                 * 这里会新建了一个页面，要切换window
                 */

                Set<String> allWindows = driver.getWindowHandles();

                int tryMaxCount = 4;
                int tryCount = 0;

                while(!isTargetWindowOpened && tryCount < tryMaxCount){
                    System.out.println("cccccccccccccccccccc");

                    System.out.println("windows size = " + allWindows.size());

                    for (String s : allWindows) {
                        System.out.println("ddddddddddddddddd");
                        String title = driver.switchTo().window(s).getTitle();
                        System.out.println("titlee = " + title);


                        if(title.equals(thirdPageTitle)){
                            System.out.println("eeeeeeeeeeeeeeeeeeeeeeeee");

                            break;
                        }//
//                        if (!driver.switchTo().window(s).getTitle().equals("中国国际贸易单一窗口")) {
//                            continue;
//                        }
                        //todo 在这里设置一个等待。

                    }


                    List<WebElement> menuItemList = driver.findElements(By.id("side-menu"));
                    System.out.println("menu list size = " + menuItemList.size());
                    if(menuItemList.size() > 0 ){
                        System.out.println("menu list size > 0");
                        isTargetWindowOpened = true;
                    }

                    if(!isTargetWindowOpened){
                        for (String s : allWindows) {
                            boolean isTitlePageArrived = false;
                            if (driver.switchTo().window(s).getTitle().equals(secondPageTitle)) {
                                isTitlePageArrived = true;
                                /**
                                 * 点击“货物申报”进去查询单子界面。
                                 */
                                driver.switchTo().frame(1).switchTo().frame(0);

                                /**
                                 * 把鼠标hover在“货物申报”按钮上面，因为selenium只能找到眼真正看到的按钮，这样后面findelemnt才找到
                                 */

                                List<WebElement> navEleList2 = driver.findElement(By.id("ywsb")).findElement(By.className("asul")).findElements(By.className("yx"));
                                for (WebElement e : navEleList2) {
                                    if (e.getText().equals("货物申报")) {
                                        System.out.println("货物申报 又移动了！！！");

                                        Actions act = new Actions(driver);
                                        act.moveToElement(e);
                                        act.build().perform();
                                        break;
                                    }
                                }


                                try {
                                    Thread.sleep(8000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                /**
                                 * 点击“货物申报”进去查询单子界面。
                                 */

                                List<WebElement> seleList2 = driver.findElement(By.id("ywsb")).findElement(By.cssSelector(".aslia.yw6"))
                                        .findElements(By.className("nrmc"));
                                for (WebElement e : seleList2) {
                                    if (e.getText().equals("货物申报")) {
                                        System.out.println("货物申报 又点击了！！！");
                                        e.click();
                                        break;

                                    }
                                }

                                try {
                                    Thread.sleep(15000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                break;

                            }
                            if(isTitlePageArrived) break;


                        }
                    }else{
                        break;
                    }
                    tryCount++;
                }


                /**
                 * 点击左边菜单列表的“数据查询统计”按钮，会生成一些按钮给我点
                 */
                List<WebElement> menuItemList = driver.findElements(By.className("nav-label"));
                for (WebElement e : menuItemList) {
//                数据查询/统计
                    if (e.getText().equals("数据查询/统计")) {
                        e.click();
                        /**
                         *这里等个一两秒，等js生成html的东西，我要点击的按钮也是生成的。
                         */
                        WebElement accessItemUlTag = new WebDriverWait(driver, kWaitTimeLong).until(new ExpectedCondition<WebElement>() {
                            @NullableDecl
                            @Override
                            public WebElement apply(@NullableDecl WebDriver webDriver) {
                                return driver.findElement(By.xpath("//ul[@class='nav nav-third-level collapse in']"));
                            }
                        });
                        List<WebElement> accessItemList = accessItemUlTag.findElements(By.tagName("li"));
                        for (WebElement ae : accessItemList) {

                            if (ae.getText().equals("报关数据查询")) {
                                ae.click();
                            }
                        }
                    }
                }

                /**
                 * 点击了“报关数据查询”之后，右边弹出新页面，点击“本周”radio按钮
                 *
                 */
                driver.switchTo().frame(1);
                WebElement queryRoot = driver.findElement(By.id("cus_basic_query"));
                WebElement querySelcRoot = queryRoot.findElement(By.className("col-sm-8"));
                List<WebElement> inputRoots = querySelcRoot.findElements(By.className("itswInput"));
                for (WebElement e : inputRoots) {
//                    List<WebElement> iTagList = e.findElements(By.tagName("i"));
                    List<WebElement> operateDate2List = e.findElements(By.id("operateDate2"));
                    if (operateDate2List.size() > 0) {
                        operateDate2List.get(0).click();  ////markmarkmarkmark 测试 注释就删掉本周点击
                    }
                }


                try {
                    Thread.sleep(8000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

//                这里要做一个吧下一页爬取的东西。因为本来的页面是只能爬五页的。
                /**
                 * 筛选自定义属性data-index，遍历完之后就下一页
                 */
                JSONObject jsonRoot = new JSONObject();
                JSONArray dataArr = new JSONArray();
                jsonRoot.put("data", dataArr);
                jsonRoot.put("crawler_start_time", System.currentTimeMillis());

//                String[] dateSpanBegins = { "2020-05-01", "2020-05-08"};
//                String[] dateSpanEnds = { "2020-05-07", "2020-05-12"};

                String[] iEFlagList = {"I", "E"};
                int[] isCustomClosedFlagList = {0, 1};
//                String[] iEFlagList = {"I"};
//                String[] iEFlagList = {"E"};
//                markmarkmarkmark 测试 时间间隔下载
//                for(int dateI = 0; dateI < dateSpanBegins.length; dateI++){
//                    JavascriptExecutor executorDate = (JavascriptExecutor)driver;
//
//                    String dateBegin = dateSpanBegins[dateI];
//                    String dateEnd = dateSpanEnds[dateI];
//
//                    executorDate.executeScript("document.getElementById('updateTime1').value = '"+ dateBegin +"'");
//                    executorDate.executeScript("document.getElementById('updateTimeEnd1').value = '"+ dateEnd +"'");


                    //todo 这里还需要加一层遍历，遍历是否结关。 markmarkmarkmarkmark
    //               todo 这里做个海关编号列表，记住下载过的海关编号.
                    //todo
                    List<String> accessedEntLst = new ArrayList<>();
                    for(int flagIndex = 0; flagIndex < iEFlagList.length; flagIndex++){
    //                    if(flagIndex > 0) break; //markmarkmarkmarkmarkmark 删
                        String iEFlag = iEFlagList[flagIndex];
                        JavascriptExecutor executor = (JavascriptExecutor)driver;
                        executor.executeScript("document.getElementById('cusIEFlag1').setAttribute('value', '" + iEFlag + "') ");
                        for(int isClosedIndex = 0; isClosedIndex < isCustomClosedFlagList.length; isClosedIndex++){
    //                        if(isClosedIndex > 0) break; //markmarkmarkmarkmarkmark 删

                            int isClosedFlag = isCustomClosedFlagList[isClosedIndex];
                            executor.executeScript("document.getElementById('tableFlag').setAttribute('value', '" + isClosedFlag + "') ");

                            //todo 在这里写是否结关迭代。
                            //“查询”按钮点击。
                            WebElement confirmBtn = driver.findElement(By.xpath("//button[@class='btn btn-sm btn-primary']"));
                            confirmBtn.click();

                            //这里应该等一下，因为从“入口”页面切换到“出口”要等加载，如果不等
                            //这里会出问题，因为上面的click之后新页面还没来就做下面的获取element操作，
                            //结果会获取失败报错崩掉。
                            try {
                                Thread.sleep(15000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            crawlAllPage(jsonRoot, iEFlag, accessedEntLst);
                        }
                    }
//                    markmarkmarkmark  测试  时间 间隔下载
//                }
                logger.info("写入到json文件>>>>>>>>>>>");
                logger.info(jsonRoot.toString());

                String downFilePath = "C:\\CAI\\crawler\\" + userName;
                File fileDirectory = new File(downFilePath);
                if (!fileDirectory.exists()) {
                    fileDirectory.mkdirs();
                }
                File file = new File(downFilePath + "\\somejsondata.json");
                FileWriter writer = new FileWriter(file);
                System.out.println("json root = " + jsonRoot.toString());
                writer.write(jsonRoot.toString());
                writer.flush();
                writer.close();
            }
        } catch (IOException | TesseractException | JSONException e) {
            e.printStackTrace();
            System.out.println("error occurred!!!!!");
            logger.error("error", e);
            logger.error(e.getMessage());
        } finally {
            System.out.println("chrome quit!!!!!!!!!!!!!!");
            cookieToSet = "";
            driver.quit();
            try {

                Runtime.getRuntime().exec("cmd /c start \"\" c:\\CAI\\terminatechrome.bat");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            Thread.sleep(40000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


//        try {
//            Thread.sleep(50000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        driver.quit();
    }


    private void crawlAllPage(JSONObject jsonRoot, String iEFlag, List<String> accessedEntLst) throws JSONException{
        /**
         * 这里有个等待加载的地方。点击插叙那之后要等一下下面的单子列表加载
         */
        WebElement tbRootTag;
        WebElement queryResultRoot = driver.findElement(By.id("cus_declare_table_div"));
        List<WebElement> pageNumberTagList = queryResultRoot.findElements(By.xpath("//li[contains(@class, 'page-number')]"));

        List<WebElement> pageInfoList = queryResultRoot.findElements(By.className("pagination-info"));
        String pageInfoStr =  pageInfoList.get(0).getText();

        int entryTotal = 0;
        Pattern pageInfoPattern = Pattern.compile("[0-9]+");


        Matcher pageInfoMatcher = pageInfoPattern.matcher(pageInfoStr);
        if(pageInfoMatcher.find()){
            entryTotal = Integer.valueOf(pageInfoMatcher.group(0));
        }

        int pageTotal = (int)Math.ceil(entryTotal / 10.0);


        int curPage = 0;
        //遍历页面page
        while (curPage < pageTotal) {
//        while (curPage == 0) { //markmarkmarkmarkmarkmark 删
            //问题是这里还是拿回上一个driver
            tbRootTag = waitAndGetCutomDataRoot(driver);

            List<WebElement> trList = tbRootTag.findElements(By.tagName("tr"));

            //遍历每一行的单子
            for (int i = 0; i < trList.size(); i++) {
//                if(accessedEntLst.size() > 0 ) break;  //markmarkmarkmarkmarkmark 删
                WebElement trTag = trList.get(i);
                List<WebElement> tdList = trTag.findElements(By.tagName("td"));

                if (tdList.size() == 1 ) break;
                //markmark todo 这里有问题
                WebElement customItemData = tdList.get(1);
                WebElement aTag = customItemData.findElement(By.tagName("a"));
                WebElement uTag = aTag.findElement(By.tagName("u"));
                String uniId = uTag.getText();
                String firstStr = String.valueOf(uniId.charAt(0));
                if(!firstStr.equals("I") && !firstStr.equals("E")) break;
                System.out.println("-------------- uni id = --------------");
                System.out.println(uniId);
                WebElement formDateElem = tdList.get(6);
                String formDate = formDateElem.getText();
                WebElement customComElem = tdList.get(12);
                String customComText = customComElem.getText();
                System.out.println("单子日期 = " + formDate);
                System.out.println("单子公司 = " + customComText);
                System.out.println("开始时间： " + mStartTime);
                WebElement customNum = tdList.get(2);
                String customNumText = customNum.getText();
                System.out.println("-------------- custom num 海关编号 = --------------");
                System.out.println(customNumText);

                System.out.println("before filter!!!!!!!*************");
                System.out.println("custom num text = " + customNumText);
                if(accessedEntLst.contains(customNumText) || customNumText.length() < 5) {
                    System.out.println("filtered!!!!!!!!!!!!!!!!!!!!!!!");
                    continue;
                }
                accessedEntLst.add(customNumText);

                /**
                 * 这里把东西滚到
                 */

                try {
                    if(DEBUG == true){
                        Thread.sleep(4000);
                    }else{
                        Thread.sleep(4000);
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                /****
                   *第一次点进去单子
                 */
                driver.manage().timeouts().implicitlyWait(kWaitTimeImplicit, TimeUnit.SECONDS);
                Actions actions = new Actions(driver);
                actions.moveToElement(customItemData).build().perform();
                //markmark 这里报错了。
                //点击“单子”
                customItemData.click();


                try {
//                    Thread.sleep(15000);
                    if(DEBUG == true){
                        Thread.sleep(10000);
                    }else{
                        Thread.sleep(10000);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                /**
                 *  这里下面注释先不要删掉，下面是避免页面崩掉 关了再开
                 */

//                driver.switchTo().defaultContent();
//                WebElement closeBtn2 = new WebDriverWait(driver, kWaitTimeShort).until(new ExpectedCondition<WebElement>() {
//                    @NullableDecl
//                    @Override
//                    public WebElement apply(@NullableDecl WebDriver webDriver) {
//                        List<WebElement> closeTagList = driver.findElements(By.xpath("//i[@class='fa fa-times-circle']"));
//                        return closeTagList.get(1);
//                    }
//                });
//                closeBtn2.click();
//
//
//                driver.switchTo().defaultContent();
//                driver.switchTo().frame(1);
//
//                try {
//                    Thread.sleep(kTestWaitTime);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//
//                /****
//                 *第二次点进去单子
//                 */
//                tbRootTag = waitAndGetCutomDataRoot(driver);
//                List<WebElement> trList2 = tbRootTag.findElements(By.tagName("tr"));
//
//                //遍历每一行的单子
////                if(i==1) break;//markmarkmarkmark 删掉 测试
//                WebElement trTag2 = trList2.get(i);
//                List<WebElement> tdList2 = trTag2.findElements(By.tagName("td"));
//                WebElement customItemData2 = tdList2.get(1);
//                driver.manage().timeouts().implicitlyWait(kWaitTimeImplicit, TimeUnit.SECONDS);
//                Actions actions2 = new Actions(driver);
//                actions2.moveToElement(customItemData2).build().perform();
//                //markmark 这里报错了。
//                //点击“单子”
//                customItemData2.click();
//
//                try {
////                    Thread.sleep(15000);
//                    Thread.sleep(10000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }

                /**
                 * 这里等待一会，才能swtich到点击单子后弹出的新frame。
                 */
                driver.switchTo().defaultContent();

                System.out.println("wait frame to be available.... before");
                new WebDriverWait(driver, kWaitTimeLong).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(2));
                System.out.println("wait frame to be available.... after");


                /**
                 * 出口单子会弹出一个框框，这里如果发现，先干掉。
                 */

                List<WebElement> dialogCloseList = driver.findElements(By.className("layui-layer-setwin"));
                System.out.println("dia log list size =  " + dialogCloseList.size());
                if(dialogCloseList.size() > 0){
                    dialogCloseList.get(0).click();
                }

                try {
                    Thread.sleep(15000);
//                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                WebElement customDataOpenedIndic = new WebDriverWait(driver, kWaitTimeLong).until(new ExpectedCondition<WebElement>() {
                    @NullableDecl
                    @Override
                    public WebElement apply(@NullableDecl WebDriver webDriver) {
                        return webDriver.findElement(By.id("dec_head_form"));
                    }
                });

                System.out.println("wait dec_head_form element after");


                //todo markmark 下面删掉
                try {
                    Thread.sleep(kTestWaitTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                /**
                 * 上面已经等完单子加载了，可以开始爬取真正单子数据。
                 */

                Set<Cookie> customCookieSet = driver.manage().getCookies();
                cookieToSet = "";
                for (Cookie c : customCookieSet) {
                    cookieToSet = cookieToSet + c.getName() + "=" + c.getValue() + ";";
                }
                String urlStr = HTTPS_HOST + "/swProxy/decserver/sw/dec/merge/queryDecData";
                System.out.println("urlStr = " + urlStr);
                String postData = "{\"cusCiqNo\":\"" + uniId + "\",\"cusIEFlag\":\"" + iEFlag + "\",\"operationType\":\"cusEdit\"}";

                //todo markmark这里停一下看看抓取的包是什么
                String result = post(urlStr, postData, cookieToSet);
                if (result == null) {
                    System.out.println("retConn null!!!!!!!!!!!!");


                }
//                logger.info("ret conn = " + result);
//                logger.info("cookies set = " + cookieToSet);

//                        String sb = getResponse(retConn);
//                logger.info("json_query_url=" + urlStr);
//                logger.info("post_data=" + postData);

//                logger.info("aaaaa_json=" + result);
                //todo markmark 这里报错了。

                //todo 这里是获取单子信息的，明显是服务器改了
                JSONObject rawObj = new JSONObject(result);
                JSONObject fileRootObj = new JSONObject();
                JSONObject entryData = new JSONObject();

                boolean isFormDataReced = rawObj.getJSONObject("data").has("preDecHeadVo");
                boolean isAvailable = false;
                if(isFormDataReced){
                    JSONObject dataObj = rawObj.getJSONObject("data").getJSONObject("preDecHeadVo");

                    //统一编号
                    String cusCiqNo = dataObj.getString("cusCiqNo");//uni_id  (_id)
                    //海关编号
                    String entryId = "";
                    if (dataObj.has("entryId")) {
                        entryId = dataObj.getString("entryId");//customId
                    }

                    //单子状态  保存 审核  放行
                    String status = dataObj.getString("cusDecStatusName");//status

                    //发货人（委托人）公司编号
                    String consigneeCode = dataObj.getString("ownerCode");//consignorCode
                    //发货人（委托人）公司scc编号
                    String consigneeScc = dataObj.getString("ownerScc");//consigneeScc
                    //发货人（委托人）公司名字
                    String consigneeCname = dataObj.getString("ownerName");//consignorName

                    //代理公司编号
                    String agentCompanyCode = dataObj.getString("agentCode");//agentCompanyCode
                    //代理公司scc编号
                    String agentCompanyScc = dataObj.getString("agentScc");//agentCompanyScc
                    //代理公司名字
                    String agentCompanyName = dataObj.getString("agentName");//agentCompanyName

                    String updateTime = dataObj.getString("updateTime");//最近更新时间

                    String dDate = "no value";
                    if(dataObj.has("dDate")){
                        dDate = dataObj.getString("dDate");
                    }

                    String ciqIEFlag = dataObj.getString("ciqIEFlag");

                    //文件列表
                    String files = "";

                    isAvailable =  dataObj.has("entryTypeName");
                    if(isAvailable){
                        String entryTypeName = dataObj.getString("entryTypeName");//entryTypeName
                        String customMasterName = dataObj.getString("customMasterName");//申报海关地

                        entryData.put("id", cusCiqNo);
                        System.out.println("*#*#*#*#*#*##*#*#* \n entryData " + cusCiqNo+ " \n *#*#*#*#*#*#*#*#*#");

                        entryData.put("customId", entryId);
                        entryData.put("status", status);
                        entryData.put("consignorCode", consigneeCode);
                        entryData.put("consignorScc", consigneeScc);
                        entryData.put("consignorName", consigneeCname);
                        entryData.put("agentCompanyCode", agentCompanyCode);
                        entryData.put("agentCompanyScc", agentCompanyScc);
                        entryData.put("agentCompanyName", agentCompanyName);
                        entryData.put("updateTime", updateTime);
                        entryData.put("entryTypeName", entryTypeName);
                        entryData.put("customMasterName", customMasterName);
                        entryData.put("dDate", dDate);
                        entryData.put("ciqIEFlag", ciqIEFlag);

//                    entryTypeName 是不是通关无纸化，从这里拿一下
                        /**
                         * 这里判断一下如果不是通关无纸化类型的单子，就没有“随单数据的”
                         */
                        if (entryTypeName.equals("通关无纸化")) {
                            /**
                             * 点击“随单数据”完成，因为单子页面刚加载的时候这个按钮是不可点的，后面他用js又改成可点，所以要加个等待
                             */

                            try {
                                if(DEBUG == true){
                                    Thread.sleep(16000);
                                }else{
                                    Thread.sleep(13000);//这个不能少，等单子加载完了才开始点随单数据
                                }

                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            WebElement attachmentBtn = driver.findElement(By.id("decDocBtn"));

                            new WebDriverWait(driver, kWaitTimeShort).until(ExpectedConditions.elementToBeClickable(By.id("decDocBtn")));

                            Actions decDocClickAct = new Actions(driver);
                            decDocClickAct.moveToElement(attachmentBtn).click().build().perform();

                            WebElement tbody = driver.findElement(By.id("decDocTable")).findElement(By.tagName("tbody"));
                            List<WebElement> fileTRTagList = tbody.findElements(By.tagName("tr"));
                            for (WebElement tre : fileTRTagList) {
                                List<WebElement> fileTDList = tre.findElements(By.tagName("td"));
                                //看看请求要什么参数这里对应的td拿一下。
                                if (fileTDList.size() >= 4) {
                                    WebElement typeTD = fileTDList.get(0);
                                    String typeStr = typeTD.getText();

                                    WebElement fileNameTD = fileTDList.get(1);
                                    String fileNameStr = fileNameTD.getText();
                                    WebElement fileNoTD = fileTDList.get(2);
                                    String fileNoStr = fileNoTD.getText();

                                    WebElement downTD = fileTDList.get(3);
                                    List<WebElement> downBtnList = downTD.findElements(By.tagName("button"));
//                                //todo markmark 这个是下载按钮，这里要拿一下onclick的属性值
                                    if (downBtnList.size() > 1) {
                                        String downClickVal = downBtnList.get(1).getAttribute("onclick");
                                        Pattern p = Pattern.compile("\'.*\'");
                                        Matcher m = p.matcher(downClickVal);
                                        m.find();
                                        downClickVal = m.group();
                                        downClickVal = downClickVal.replaceAll("'", "");
                                        String fileActualName = new String(downClickVal);


                                        setDownloadFileObj(typeStr,  fileNameStr, fileNoStr, fileRootObj, fileActualName);
                                        String downFilePath = "attachments/" + uniId;
                                        File fileDirectory = new File(downFilePath);
                                        if (!fileDirectory.exists()) {
                                            fileDirectory.mkdirs();
                                        }

                                        String downUrl = kHost + "/swProxy/decserver/sw/doc/cus/download/" + downClickVal;
//                                      /swProxy/decserver/sw/doc/cus/download/2019090917080472776419

                                        String filePath = downFilePath + "/" + downClickVal + ".pdf";

                                        downloadFile(downUrl, filePath, cookieToSet);
//downloadFile(String url, String filePath, String cookieToSet, String customApplyFileName, JSONObject fileRootObj) throws JSONException{
                                        try {
                                            if(DEBUG == true){
                                                Thread.sleep(5000);
                                            }else{
                                                Thread.sleep(10000);
                                            }
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }

                        }
                    }

                }

                driver.switchTo().defaultContent();
                WebElement closeBtn = new WebDriverWait(driver, kWaitTimeShort).until(new ExpectedCondition<WebElement>() {
                    @NullableDecl
                    @Override
                    public WebElement apply(@NullableDecl WebDriver webDriver) {
                        List<WebElement> closeTagList = driver.findElements(By.xpath("//i[@class='fa fa-times-circle']"));
                        return closeTagList.get(1);
                    }
                });
                closeBtn.click();
//
//                    //todo markmark 下面删掉
                try {
                    Thread.sleep(kTestWaitTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
//
//                    /**
//                     * 爬取完了之后就点击“叉叉”，回到之前的单子列表页面。
//                     */
                driver.switchTo().defaultContent();
                driver.switchTo().frame(1);

                if(isFormDataReced && isAvailable){

                    String customDownUrl = kHost + "/swProxy/decserver/entries/ftl/1/0/0/" + uniId + ".pdf";
                    String downFilePath = "attachments/" + uniId;
                    String customApplyFileName = "报关单_" + uniId + ".pdf";
                    String relCustomDownPath = downFilePath + "/" + customApplyFileName;

                    setDownloadFileObj("",  customApplyFileName, "", fileRootObj, customApplyFileName);
                    downloadFile(customDownUrl, relCustomDownPath, cookieToSet);

                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }



                    String notiUrl = HTTPS_HOST + "/swProxy/decserver/sw/dec/common/queryCusDecRetByList";
//                                               /swProxy/decserver/sw/dec/common/queryCusDecRetByList
                    JSONObject notiQueryJSON = new JSONObject();
                    JSONArray notiQueryArr = new JSONArray();
                    notiQueryArr.put(uniId);
                    notiQueryJSON.put("paraList", notiQueryArr);
//                这里要改一下：改成po，然后下载是一个列表。这里明明就用那个文件名好了
//                String notiUrlPre = HTTPS_HOST + "/swProxy/decserver/sw/dec/common/queryCusDecRet?cusCiqNo=";
////                                                      oxy/decserver/sw/dec/common/queryCusDecRetBy
//                String notiUrlSur = "&limit=100&offset=0&0order=desc&stName=noticeDate";
//                String queryNotiUrl = notiUrlPre + uniId + notiUrlSur;

//                        HttpURLConnection queryNotiConn = get(queryNotiUrl, cookieToSet);
//                        String queryNotiRet = getResponse(get(queryNotiUrl,cookieToSet));
                    String postVal = post(notiUrl, notiQueryJSON.toString(), cookieToSet);
                    if(postVal != null){
                        JSONArray queryNotiArr = new JSONArray();
                        if(queryNotiArr.length() >0){
                            JSONArray rowsArr = queryNotiArr.getJSONObject(0).getJSONArray("list");
                            String notiNo = "";
                            for (int j = 0; j < rowsArr.length(); j++) {
                                JSONObject entry = rowsArr.getJSONObject(j);

                                notiNo = entry.getString("cusRetSeqNo");
                                if (!notiNo.isEmpty()) {
                                    String notiDownUrl = HTTPS_HOST + "/swProxy/decserver/ftlNotice/ftl/" + notiNo + ".pdf";
                                    String notiDownFilePath = "attachments/" + uniId;
                                    String notiFileName = "通知书_" + notiNo + ".pdf";
                                    String relNotiDownPath = notiDownFilePath + "/" + notiFileName;

                                    setDownloadFileObj("",  notiFileName, "", fileRootObj, notiFileName);
                                    downloadFile(notiDownUrl, relNotiDownPath, cookieToSet);
                                }

                                try {
                                    if(DEBUG == true){
                                        Thread.sleep(3000);
                                    }else{
                                        Thread.sleep(5000);
                                    }
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                entryData.put("files", fileRootObj);
                            }

                        }
                    }


                    jsonRoot.getJSONArray("data").put(entryData);
                }
            }
            /**
             * 点击下一页，这里每次都要重新找一下page标签，因为之前不招的话会出问题，最后以一个findelement就找不到了
             */
            curPage++;
            if (curPage >= pageTotal) {
                break;
            }

            WebElement queryResultRootNew = driver.findElement(By.id("cus_declare_table_div"));
            WebElement aTagEle = queryResultRootNew.findElement(By.className("page-next")).findElement(By.tagName("a"));
            aTagEle.click();


            /**
             * 点击下一页的时候，原来的tr元素会删掉，然后回异步下载实例化新的tr
             * 所以这里隔一段时间判断是否原来的tr元素已经没了。才能开始findelement新的tr
             */
            try {
                if(DEBUG == true){
                    Thread.sleep(8000);
                }else{
                    Thread.sleep(20000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }


    /**
     * 这里用fiddle抓urlconnection的包一定要设置下面
     */
    private static void setSystemConfig() {
//        System.setProperty("http.proxyHost", "localhost");
//        System.setProperty("http.proxyPort", "8888");
//        System.setProperty("https.proxyHost", "localhost");
//        System.setProperty("https.proxyPort", "8888");

        System.setProperty("http.proxyHost", "127.0.0.1");
        System.setProperty("https.proxyHost", "127.0.0.1");
        System.setProperty("http.proxyPort", "8888");
        System.setProperty("https.proxyPort", "8888");
    }

    private WebElement waitAndGetCutomDataRoot(WebDriver driver) {
//        WebElement tbRootTag =
        return new WebDriverWait(driver, kWaitTimeLong).until(new ExpectedCondition<WebElement>() {
            @NullableDecl
            @Override
            public WebElement apply(@NullableDecl WebDriver webDriver) {
                WebElement tableRoot = webDriver.findElement(By.id("declareTable"));
                WebElement tbRoot = tableRoot.findElement(By.tagName("tbody"));
                List<WebElement> trList = tbRoot.findElements(By.tagName("tr"));
                if (trList.size() >= 1) {
                    if (trList.get(0).findElements(By.tagName("td")).size() >= 1) {
                        return tbRoot;
                    }
                }
                return null;
            }
        });
    }

    private void setDownloadFileObj(String type, String fileName, String fileNo, JSONObject fileRootObj, String keyName) throws JSONException{
        JSONObject fileDetailObj = new JSONObject();
        fileDetailObj.put("type", type);
        fileDetailObj.put("file_name", fileName);
        fileDetailObj.put("file_no", fileNo);

        String[] fileNameDeparteds = keyName.split("\\.");
        if(fileNameDeparteds.length > 1){
            keyName = fileNameDeparteds[0] + fileNameDeparteds[1];
        }

        fileRootObj.put(keyName, fileDetailObj);

        System.out.println("file root obj=================");
        System.out.println(fileRootObj.toString());
        System.out.println("开始时间： " + mStartTime);
    }

    private void downloadFile(String url, String filePath, String cookieToSet) throws JSONException{
        System.out.println("start to url!!!");
        try {
            Response response = okHttpClient.newCall(new Request.Builder()
                    .url(url)
                    .header("Cookie", cookieToSet)
                    .build()).execute();

            if (response.isSuccessful()) {
                InputStream in = response.body().byteStream();
                FileOutputStream fos = new FileOutputStream(filePath);

                int byteRead = -1;
                byte[] buffer = new byte[1024];
                while((byteRead = in.read(buffer)) != -1){
                    fos.write(buffer, 0, byteRead);
                }

                fos.close();
                in.close();

//                return response.body() != null ? response.body().string() : null;
            } else {
//                logger.warn("request post fail, url: {}, postData: {}", url, postData);
                logger.warn("request post fail code: {}", response.code());
                logger.warn("request post fail message: {}", response.body() != null ? response.body().string() : null);
                response.body().close();
            }
        } catch (IOException e) {
            logger.error("request error: {}", e.getMessage());
        }

//        URL downFileUrl = new URL(url);
//        HttpURLConnection downConn = (HttpURLConnection) downFileUrl.openConnection();
//        downConn.setDoOutput(true);
//        downConn.setRequestProperty("Cookie", cookieToSet);
//        InputStream downInputStream = downConn.getInputStream();
//
//        FileOutputStream downOutputStream = new FileOutputStream(filePath);
//
//        int byteRead = -1;
//        byte[] buffer = new byte[1024];
//        while ((byteRead = downInputStream.read(buffer)) != -1) {
//            downOutputStream.write(buffer, 0, byteRead);
//        }
//
//        downOutputStream.close();
//        downInputStream.close();



    }

    private String post(String urlStr, String postData, String cookieToSet) {
        try {
            Response response = okHttpClient.newCall(new Request.Builder()
                    .url(urlStr)
                    .post(RequestBody.create(MediaType.parse("application/json; charset=utf-8"), postData))
                    .header("Cookie", cookieToSet)
                    .build()).execute();

            if (response.isSuccessful()) {

                return response.body() != null ? response.body().string() : null;
            } else {
                logger.warn("request post fail, url: {}, postData: {}", urlStr, postData);
                logger.warn("request post fail code: {}", response.code());
                logger.warn("request post fail message: {}", response.body() != null ? response.body().string() : null);
                response.body().close();
            }
        } catch (IOException e) {
            logger.error("request error: {}", e.getMessage());
        }


        return null;
    }

    private String get(String urlStr, String cookieToSet) {
        try {
            Response response = okHttpClient.newCall(new Request.Builder()
                    .url(urlStr)
                    .addHeader("Cookie", cookieToSet)
                    .build()).execute();

            if (response.isSuccessful()) {
                return response.body() != null ? response.body().string() : null;
            } else {
                logger.warn("request get fail, url: {}", urlStr);
                logger.warn("request get fail code: {}", response.code());
                logger.warn("request get fail message: {}", response.body() != null ? response.body().string() : null);
                response.body().close();
            }
        } catch (IOException e) {
            logger.error("request error: {}", e.getMessage());
        }

//        try {
//            URL urlPost = new URL(urlStr);
//            HttpURLConnection connection = (HttpURLConnection) urlPost.openConnection();
//            connection.setDoOutput(true);
//            connection.setRequestMethod("GET");
//            connection.setRequestProperty("Content-type", "application/json;charset=UTF-8");
//            connection.setRequestProperty("Cookie", cookieToSet);
//
//            return connection;
//        } catch (IOException e) {
//
//        }
        return null;
    }

    private String getResponse(HttpURLConnection connection) {
        try {
//            Map<String,List<String>>
            Map<String, List<String>> maps = connection.getHeaderFields();
//            for(Map.Entry<String, List<String>>)
            List<String> locList = maps.get("Location");
            String newUrl = connection.getHeaderField("Location");

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
            String line;
            StringBuilder sb = new StringBuilder();

            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            reader.close();

            return sb.toString();
        } catch (IOException e) {

        }

        return null;

    }

    /**
     * 下面代码是截屏代码，之前用来截取验证码用的，现在用cookie请求可以了就废弃了。以后删掉。
     */
    private void screenShotCode() {
//            /****
//             * 这里是截屏得到的验证码，没什么用了，后面删掉。
//             */
//            File outputFile2 = new File("screenshot.png");
//            File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//            org.openqa.selenium.Point point = image.getLocation();
//            //    // Get entire page screenshot
//            BufferedImage fullImg = ImageIO.read(screenshot);
//            // Get the location of element on the page
//            // Get width and height of the element
//            int eleWidth = image.getSize().getWidth();
//            int eleHeight = image.getSize().getHeight();
//            // Crop the entire page screenshot to get only element screenshot
//            BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth, eleHeight);
//            ImageIO.write(eleScreenshot, "png", screenshot);
//            FileUtils.copyFile(screenshot, outputFile2);

    }

}
