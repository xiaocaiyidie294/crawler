package cn.caizhenhang.crawler;

import java.io.*;

public class CommonUtil {
    public static final String kUserListFilePath = "C:\\CAI\\crawler\\userlist.json";


    public static String fileToString(String fileName){
        try{
            File tocrawlfile = new File(fileName);
            tocrawlfile.createNewFile();
            FileInputStream fin = new FileInputStream(tocrawlfile);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while((line = reader.readLine())!= null){
                sb.append(line);
            }
            reader.close();
            fin.close();

            String toCrawlListStr = sb.toString();
            return toCrawlListStr;
        }catch(IOException e){
            e.printStackTrace();
        }
        return null;

    }

    public static void stringToFile(String str, String filePath){
        try{
            File file = new File(filePath);
            FileWriter writer = new FileWriter(file);
            writer.write(str);
            writer.flush();
            writer.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
