package cn.caizhenhang.crawler;

import net.sourceforge.tess4j.Tesseract;

import java.nio.file.Path;
import java.nio.file.Paths;

public class OCR {
    private static Tesseract tesseract;
//    private static final String tessDataRelPath = "tessdata/eng.traineddata";
    private static final String tessDataRelPath = "tessdata";

    public static Tesseract getTesseract(){
        String curDir = System.getProperty("user.dir");
        Path path = Paths.get(curDir, tessDataRelPath);
        System.out.println("11111111111111");
        System.out.println(path.toString());

        if(tesseract == null){
            tesseract = new Tesseract();
//            tesseract.setDatapath(path.toString());
            tesseract.setDatapath("C:\\CAI\\ocr\\tessdata");
            tesseract.setLanguage("eng");
        }

        return tesseract;
    }
}
