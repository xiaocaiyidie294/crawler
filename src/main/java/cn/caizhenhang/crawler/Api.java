package cn.caizhenhang.crawler;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;

@RestController
public class Api {
    Logger logger = LoggerFactory.getLogger(CrawlerTask.class);

    @PostMapping(value = "/getJson")
    //首先爬虫那边要做成根据那边传过来的username 和 密码来爬取。并且存在某个文件夹，根据username作为文件夹名
    //
    public Object getJson(@RequestParam("token") String token, @RequestParam("username") String username, @RequestParam("passwd") String passwd){
        if("cai".equals(token)){
            try {
                //这里看看json文件有没有那个账号密码先。
                //如果已经有了，那么就开始获取json
                //如果没有，创建一个词条账号密码，这样爬虫就会隔一段时间自动根据这个json获取
//                markmark看看怎么读取json文件。
                String userListStr = CommonUtil.fileToString(CommonUtil.kUserListFilePath);
                JSONArray ja = new JSONArray();
                if(!userListStr.isEmpty()){
                    ja = new JSONArray(userListStr);
                }

                //todo 看看jsonlist文件有没有这个username ，没有就创建，
                boolean isContained = false;


                for(int i = 0; i < ja.length(); i++){
                    JSONObject jo = ja.getJSONObject(i);
                    String itemname = jo.getString("username");

                    if(itemname.equals(username)){
                        jo.put("passwd", passwd);
                        isContained = true;
                    }
                }

                if(!isContained){
                    //这里判断看看有没有文件夹， 有就返回。不用看文件有没有，因为文件是跟文件夹一起
                    //创建的。那边带着账号密码过来请求相应的数据，无论有没有都好，都要更新账号密码。
                    JSONObject newjo = new JSONObject();

                    newjo.put("username", username);
                    newjo.put("passwd", passwd);
                    ja.put(newjo);

                    CommonUtil.stringToFile(ja.toString(), CommonUtil.kUserListFilePath);
                }else{

//                    既然文件列表有，那么就判断是否有文件夹，有就肯定有数据，就返回。
                    String downFileDirPath = "C:\\CAI\\crawler\\" + username;
                    File fileDirectory = new File(downFileDirPath);
                    if(fileDirectory.exists()){
                        String jsonDataPath = downFileDirPath + "\\somejsondata.json";
                        InputStream inputStream = new FileInputStream(jsonDataPath);
                        String text = IOUtils.toString(inputStream,"gbk");

                        return com.alibaba.fastjson.JSONObject.parseObject(text);
                    }

                }

            } catch (IOException | JSONException e) {
                e.printStackTrace();

                return e.getMessage();
            }
        }

        return "no Token";
    }

}
