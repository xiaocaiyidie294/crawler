package cn.caizhenhang.crawler;

import com.alibaba.fastjson.JSONObject;
import org.junit.Test;

import java.util.Scanner;

public class MainTests {
    @Test
    public void test(){

    }

    public static void main(String args[]){
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()){
            String line = scanner.nextLine().trim();
            int index = line.indexOf("=");
            if(index==-1) continue;
            String left = line.substring(0 , index-1);
            String right = line.substring(index+2 , line.length()-1);
            String result = right + " = " + left + ";";
            System.out.println(result);
        }
    }
}
