package cn.caizhenhang.crawler;

import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CrawlerApplicationTests {

	/* 让Spring管理App2，自行new出来会导致OkhttpClient无法注入 */
	@Autowired
	App2 app;

	@Test
	public void contextLoads() {
		try{
			String userListStr = CommonUtil.fileToString(CommonUtil.kUserListFilePath);


			JSONArray ja = new JSONArray(userListStr);
			System.out.println("userListStr = " + userListStr.toString());
			System.out.println("@@@@@@@@@@@@@@@@@@@ len = " + ja.length());
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			LocalDateTime now = LocalDateTime.now();
			String startTime = dtf.format(now);

			for(int i = 0; i < ja.length(); i++){
				System.out.println("i  = " + i);
				JSONObject jo = ja.getJSONObject(i);
				String username = jo.getString("username");
				System.out.println("^^^^^^^^^^^^^^^^^^^^  username" + username);
				String passwd = jo.getString("passwd");
				System.out.println("^^^^^^^^^^^^^^^^^^^^  passwd" + passwd);


				app.start(username, passwd, startTime);

			}

		}catch(JSONException e){
			e.printStackTrace();
		}

	}

	@Autowired
	OkHttpClient okHttpClient;

	@Test
	public void post(){
		String urlStr = "https://www.singlewindow.gd.cn/swProxy/decserver/sw/dec/merge/queryDecData";
		String postData = "{\"cusCiqNo\":\"I20190000274660017\",\"cusIEFlag\":\"I\",\"operationType\":\"cusEdit\"}";
		String cookieToSet = "SERVERID=c663b17c753d3f9ec1523bceefcc9f23|1564811494|1564811458;JSESSIONID=E226AA05C9914983258024329C63223D;!Proxy!route1plat=a618b9fcd6d4765b6637ceb93da6dccd;!Proxy!JSESSIONID=9e9bba10-55b5-4bcd-9279-c2ebfca18b20;";

		try {
			Response response = okHttpClient.newCall(new Request.Builder()
					.url(urlStr)
					.post(RequestBody.create(MediaType.parse("application/json; charset=utf-8"),postData))
					.addHeader("Cookie",cookieToSet)
					.addHeader("Origin","https://www.singlewindow.gd.cn")
					.addHeader("User-Agent","Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36")
					.build()).execute();

			System.out.println("okhttp3 response:");
			if (response.isSuccessful()) {
				System.out.println(response.body() != null ? response.body().string() : null);
			}else{
				System.out.println(response.code());
				System.out.println(response.body() != null ? response.body().string() : null);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void post2(){
		String urlStr = "https://www.singlewindow.gd.cn/swProxy/decserver/sw/dec/merge/queryDecData";
		String postData = "{\"cusCiqNo\":\"I20190000274660017\",\"cusIEFlag\":\"I\",\"operationType\":\"cusEdit\"}";
		String cookieToSet = "SERVERID=c663b17c753d3f9ec1523bceefcc9f23|1564811494|1564811458;JSESSIONID=E226AA05C9914983258024329C63223D;!Proxy!route1plat=a618b9fcd6d4765b6637ceb93da6dccd;!Proxy!JSESSIONID=9e9bba10-55b5-4bcd-9279-c2ebfca18b20;";

		try {
			URL urlPost = new URL(urlStr);
			HttpURLConnection connection = (HttpURLConnection) urlPost.openConnection();
			if (connection == null) {
				System.out.println("null!!!!!!!!!!!!!!");
			}
			connection.setDoOutput(true);
			connection.setInstanceFollowRedirects(true);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-type", "application/json;charset=UTF-8");
			connection.setRequestProperty("X-Requested-Withe", "XMLHttpRequest");
			connection.setRequestProperty("Accept", "application/json, text/javascript, */*; q=0.01");
			connection.setRequestProperty("Connection", "keep-alive");
			connection.setRequestProperty("Cookie", cookieToSet);
			connection.setRequestProperty("rdtime", "undefined");
			connection.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
			connection.setRequestProperty("myname", "jakeli");
			connection.setRequestProperty("Accept-Language", "zh-CN,zh;q=0.9");
			connection.setRequestProperty("Content-Length", "75");
			connection.setRequestProperty("Origin", "https://www.singlewindow.gd.cn");
			connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36");
			System.out.println("postData = " + postData);
			if (connection == null) {
				System.out.println("null!!!!!!!!!!!!!!22222222222222");
			}

			OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
			writer.write(postData);
			writer.flush();
			writer.close();
			if (connection == null) {
				System.out.println("null!!!!!!!!!!!!!!33333333333332");
			} else {
				System.out.println("not notno nottnottnontontoto null");
			}

			System.out.println(connection.getResponseCode());
			System.out.println(connection.getResponseMessage());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

